<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreZoneRequest;
use App\Http\Requests\Api\UpdateZoneRequest;
use App\Http\Resources\ZoneResource;
use App\Http\Resources\ZoneCollection;
use App\Models\Zone;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Log;


class ZoneApiController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/v1/zone",
     *      operationId="listZone",
     *      tags={"Zone"},
     *      summary="List all zones",
     *      description="Returns zones data",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 example={"data": "[]", "links": {"self": "link-value"}}
     *             )
     *         )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     * )
     */
    public function index()
    {
        Log::debug("Listing the zones");
        return new ZoneCollection(Zone::paginate());
    }

    /**
     * @OA\Post(
     *      path="/api/v1/zone",
     *      operationId="storeZone",
     *      tags={"Zone"},
     *      summary="Store new zone",
     *      description="Returns zone data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"name"},
     *              @OA\Property(property="name", type="string", example="Potager"),
     *              @OA\Property(property="width", type="integer", example="0"),
     *              @OA\Property(property="height", type="integer", example="0"),
     *              @OA\Property(property="position_top", type="integer", example="0"),
     *              @OA\Property(property="position_left", type="integer", example="0"),
     *          ),  
     *     ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function store(StoreZoneRequest $request)
    {
        $Zone = Zone::create($request->all());
        return (new ZoneResource($Zone))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }
    /**
     * @OA\Get(
     *      path="/api/v1/zone/{id}",
     *      operationId="getZoneById",
     *      tags={"Zone"},
     *      summary="Get Zone information",
     *      description="Returns Zone data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Zone id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 example={
     *                      "id": 1,
     *                      "name": "Potager", 
     *                      "plants": "[]",
     *                      "tasks": "[]",
     *                  }
     *             )
     *         )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function show(string $id)
    {
        Log::debug("Show zone $id");
        $zone = Zone::findOrFail($id);
        $zone->load('plants');
        $zone->load('tasks');
        return (new ZoneResource($zone))->response();
    }

    /**
     * @OA\Put(
     *      path="/api/v1/zone/{id}",
     *      operationId="updateZone",
     *      tags={"Zone"},
     *      summary="Update existing zone",
     *      description="Returns updated zone data",
     *      @OA\Parameter(
     *          name="id",
     *          description="zone id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"name"},
     *              @OA\Property(property="name", type="string", example="Potager"),
     *          ),
     *     ),
     *      @OA\Response(
     *          response=202,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function update(string $id, UpdateZoneRequest $request)
    {
        Log::debug("Udpate zone $id");
        $zone = Zone::findOrFail($id);
        $zone->update($request->all());
        return (new ZoneResource($zone))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * @OA\Delete(
     *      path="/api/v1/zone/{id}",
     *      operationId="deleteZone",
     *      tags={"Zone"},
     *      summary="Delete existing Zone",
     *      description="Deletes a record and returns no content",
     *      @OA\Parameter(
     *          name="id",
     *          description="zone id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function destroy(String $id)
    {
        $zone = Zone::find($id);
        $http_code =  Response::HTTP_NOT_FOUND;
        if(!empty($zone)) {
            $zone->delete();
            $http_code =  Response::HTTP_NO_CONTENT;
        }
        return response(null, $http_code);
    }
}
